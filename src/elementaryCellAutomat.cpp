#include "elementaryCellAutomat.h"

const int SIZE = 800;
const int CELLSIZE = 5;
const int UPDATE_INTERVAL = 2;
const int RULE = 62;
const bool COLOR = false;

//--------------------------------------------------------------
void elementaryCellAutomat::setup(){

    ofSetWindowShape(SIZE, SIZE);

    iter = 1;
    nCell = SIZE/CELLSIZE;
    
    grid = new int [nCell*nCell];  
    colors = new int [nCell*3];  
    for (int i=0; i<nCell; i++) {
        for (int j=0; j<nCell; j++) {  
          grid[i*nCell + j] = 0; 
        }  
        colors[i*3] =   rand() % 2 && COLOR ? (-255*i/nCell)+255 : 0;
        colors[i*3+1] = rand() % 2 && COLOR ? (-255*i/nCell)+255 : 0;
        colors[i*3+2] = rand() % 2 && COLOR ? (-255*i/nCell)+255 : 0;
    }
    grid[int(nCell/2)] = 1;

    ofBackground(ofColor::white);
    ofSetBackgroundAuto(true);
    title = "Elementary Cellular Automaton (rule " + std::to_string(RULE) + ")";
    ofSetWindowTitle(title);
    ofSetFrameRate(24);
}



//--------------------------------------------------------------
int elementaryCellAutomat::computeRule(int p, int q, int r) {
    // all rules available at:
    //      http://atlas.wolfram.com/01/01/views/173/TableView.html
    
   if (RULE == 60) {
       return (p + q) % 2;
   } else if (RULE == 62) {
       return (p + q + r + p*r + q*r + p*q*r) % 2;
   } else if (RULE == 150) {
       return (p + q + r) % 2;
   } else {
       return 0;
   }

}

//--------------------------------------------------------------
void elementaryCellAutomat::update(){

    if (ofGetFrameNum() % UPDATE_INTERVAL == 0 && iter<nCell) {
        for (int j=0; j<nCell; j++) {
            L = grid[(iter-1)*nCell + j-1];
            C = grid[(iter-1)*nCell + j];
            R = grid[(iter-1)*nCell + j+1];
            grid[iter*nCell + j] = computeRule(L, C, R); 
        }
        iter++;
    }
}

//--------------------------------------------------------------
void elementaryCellAutomat::draw(){

    for (int i=0; i<nCell; i++) {
        ofSetColor(colors[i], colors[i+1], colors[i+2]);
        for (int j=0; j<nCell; j++) {  
            if (grid[i*nCell + j] == 1) {
                ofDrawRectangle(j*CELLSIZE, i*CELLSIZE, CELLSIZE, CELLSIZE);
            }  
        }
    }
}
