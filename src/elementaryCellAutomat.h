#pragma once

#include "ofMain.h"

class elementaryCellAutomat : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

    private:
        int computeRule(int p, int q, int r);
        int *grid;
        int *colors;
        int nCell;
        int iter;
        std::string title;
        int L; 
        int C;
        int R;
};
