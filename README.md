# Elementary Cellular Automaton

Implementation of the [Elementary Cellular Automaton](https://en.wikipedia.org/wiki/Elementary_cellular_automaton) in Openframeworks.


![Alt text](imgs/rule_62.png?raw=true "Example of rule 62")

![Alt text](imgs/rule_150.png?raw=true "Example of rule 150")

